# Create EKS cluster with eksctl command line tool 2024

### Pre-Requisites

AWS

eks command line

git


#### Project Outline

We can create it using the AWS CLI interface however there would still be some manual intervention required such as roles, vpc and eks cluster etc
One of the most efficient ways is to use a command line tool for working with EKS cluster to automate individual tasks
We can configure everything using one command line
Where cluster will be created using default parameter but can also customize our cluster


#### Getting started

First we can instal eksctl and connect it to our aws account

To install eksctl we can use any of the below links

https://github.com/eksctl-io/eksctl#installation

https://eksctl.io/introduction/#for-windows

https://community.chocolatey.org/packages/eksctl#install

In my case I will proceed to use choco

```
choco install -y eksctl
```

![Image2](https://gitlab.com/FM1995/create-eks-cluster-with-eksctl-command-line-tool-2024/-/raw/main/Images/Image2.png?ref_type=heads)

Checking the version

```
eksctl version
```

![Image1](https://gitlab.com/FM1995/create-eks-cluster-with-eksctl-command-line-tool-2024/-/raw/main/Images/Image1.png?ref_type=heads)

We then have to configure the AWS credentials to connect to our account and create the eks resources

```
aws configure
```

And can see we have the credentials

```
aws configure list
```

![Image3](https://gitlab.com/FM1995/create-eks-cluster-with-eksctl-command-line-tool-2024/-/raw/main/Images/Image3.png?ref_type=heads)

Can now create the eks cluster

```
eksctl create cluster \
--name demo-cluster \
--version 1.27 \
--region eu-west-2 \
--nodegroup-name demo-nodes \
--node-type t2.micro \
--nodes 2 \
--nodes-min 1 \
--nodes-max 3
```

![Image4](https://gitlab.com/FM1995/create-eks-cluster-with-eksctl-command-line-tool-2024/-/raw/main/Images/Image4.png?ref_type=heads)

Can see the creation is in progress

Upon checking the logs, can see kubectl can talk to eks

![Image5](https://gitlab.com/FM1995/create-eks-cluster-with-eksctl-command-line-tool-2024/-/raw/main/Images/Image5.png?ref_type=heads)

```
kubectl get nodes
```

![Image6](https://gitlab.com/FM1995/create-eks-cluster-with-eksctl-command-line-tool-2024/-/raw/main/Images/Image6.png?ref_type=heads)

Now lets see what got created in the AWS account

Can see two new roles have been added

![Image7](https://gitlab.com/FM1995/create-eks-cluster-with-eksctl-command-line-tool-2024/-/raw/main/Images/Image7.png?ref_type=heads)

Can also see the vpc got created

![Image8](https://gitlab.com/FM1995/create-eks-cluster-with-eksctl-command-line-tool-2024/-/raw/main/Images/Image8.png?ref_type=heads)

Can also see the subnets created

Since eu-west-2 has 3 availability zones we have 6 subnets 3 for public and 3 for private

![Image9](https://gitlab.com/FM1995/create-eks-cluster-with-eksctl-command-line-tool-2024/-/raw/main/Images/Image9.png?ref_type=heads)

Now checking the cluster

![Image10](https://gitlab.com/FM1995/create-eks-cluster-with-eksctl-command-line-tool-2024/-/raw/main/Images/Image10.png?ref_type=heads)

Can see the nodes which are ec2 instances

Can also see two ec2 instances

![Image11](https://gitlab.com/FM1995/create-eks-cluster-with-eksctl-command-line-tool-2024/-/raw/main/Images/Image11.png?ref_type=heads)









